#!/usr/bin/env python

from distutils.core import setup


setup(
    name='SeaLanderApi',
    version='0.3',
    description='an api backend for a 2 player game',
    author='Jon Wehling',
    author_email='jonathon.wehling@gmail.com',
    url='',
    packages=['landerapi'])

